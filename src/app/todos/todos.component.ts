import { Component, OnInit } from '@angular/core';
import { AngularFireDatabase, AngularFireList } from '@angular/fire/database';
import { Observable } from 'rxjs';
import { DatePipe } from '@angular/common';
import {FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthService } from '../auth.service';
import { SendbackService } from '../sendback.service';
import {catchError, flatMap} from 'rxjs/operators';

@Component({
  selector: 'todos',
  templateUrl: './todos.component.html',
  styleUrls: ['./todos.component.css']
})
export class TodosComponent implements OnInit {

  form = new FormGroup(
    {todo:new FormControl('', Validators.required)}
  )
  
  
  //todo: test if this is really needed
  trackByIndex(index: number, obj: any): any {
    return index;
  }
 
  todos: any[];
  /*
  todos: any[] = [{$key:"id1", "text":"Just a trial todo", "checked":false, "status":""},
                  {$key:"id2", "text":"Another a trial todo", "checked":true, "status":""}];
  */
  sortTodos(){
    this.todos.sort((a,b)=>b.checked - a.checked);
  }
   
  addTodo(){
      let time = new Date().toString();
      let todo = {text:this.form.value.todo, 'status':'',checked:false, createTime:time};
      this.authService.user.subscribe(
        val =>{
          let uid = val.uid;
          console.log(todo);
          this.form.get('todo').setValue('');
          this.db.list('user/'+uid +'/todos').push(todo); 
        }
      )        
  }

  editTodo($event){ 
    let updatedtodo = $event;
    let time = new Date().toString();
    let key = updatedtodo.key;
    let toedit = this.todos.find(function(element){
      return element.$key == key;
    });  
    toedit.text = updatedtodo.text;
    this.authService.user.subscribe(
      val =>{
        let uid = val.uid;
        this.db.list('user/'+uid +'/todos').update(toedit.$key,{'text':toedit.text,updateTime:time});
      }
    )            
  }


  done($event){
    //done needs to check if the todo came from
    //someone else. If it did done needs to 
    //update the status of the original todo 
    //through php 
    let updatedtodo = $event; 
    let time = new Date().toString();
    let key = updatedtodo.key;    
    //for optimistic update
    let toedit = this.todos.find(function(element){
      return element.$key == key;
    });
    toedit.checked = updatedtodo.check;
    this.authService.user.subscribe(
      val =>{
        let uid = val.uid;
        this.db.list('user/'+uid +'/todos').update(toedit.$key,{'checked':toedit.checked,updateTime:time});
        //if this todo recieved from some else
        if(toedit.senderUid){
            console.log("this info should be send to original");
            console.log(toedit); 
            this.sendbackService.sendChecked(toedit);
        }
      }
    )    
  }
 

  deleteTodo($event){
    let deletedtodo = $event;
    let key = deletedtodo.key;
    let tobedeleted = this.todos.find(function(element){
      return element.$key == key;
    })
    let index = this.todos.indexOf(tobedeleted);
    let deleted = this.todos.splice(index,1);
    this.authService.user.subscribe(
      val =>{
        let uid = val.uid;
        this.db.list('user/'+uid +'/todos').remove(tobedeleted.$key);
      }
    )    
  }

  
  constructor(private db: AngularFireDatabase, public authService: AuthService, private sendbackService:SendbackService ) { }

  ngOnInit() {
    let uid = this.authService.user.subscribe(
      val =>{
        if(!val) return;        
        let uid = val.uid;
        //let ref = this.db.database.ref();
        this.db.list('/user/' + uid + '/todos').snapshotChanges().subscribe(
          todos =>{
            this.todos = [];
            todos.forEach(
              todo => {
                let y = todo.payload.toJSON();
                y["$key"] = todo.key;
                this.todos.push(y);
              }
            )
          }
        )
      }
    )      
  }
}
