import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AngularFireDatabase, AngularFireList } from '@angular/fire/database';
import { AuthService } from '../auth.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-codes',
  templateUrl: './codes.component.html',
  styleUrls: ['./codes.component.css']
})
export class CodesComponent implements OnInit {

  form = new FormGroup(
    {code:new FormControl('', Validators.required),
    nickname:new FormControl('', Validators.required)}
  )

  codes: any[];

  addCode(){
    //refactured 
    let time = new Date().toString();
    let code = {code:this.form.value.code, nickname:this.form.value.nickname, createTime:time};
    this.authService.user.subscribe(
      val =>{
        let uid = val.uid;
        console.log(code);
        this.form.get('code').setValue('');
        this.form.get('nickname').setValue('');
        this.db.list('user/'+uid +'/codes').push(code); 
      }
    )        
}
  
  constructor(public authService: AuthService, private db: AngularFireDatabase ) { }

  ngOnInit() {
    let uid = this.authService.user.subscribe(
      val =>{
        if(!val) return;        
        let uid = val.uid;
          this.db.list('/user/' + uid + '/codes').snapshotChanges().subscribe(
          codes =>{
            this.codes = [];
            codes.forEach(
              code => {
                let y = code.payload.toJSON();
                y["$key"] = code.key;
                this.codes.push(y);
              }
            )
            console.log(this.codes);
          }
        )
      }
    )      
  }    

}
