import { AngularFireDatabase, AngularFireList } from '@angular/fire/database';
import { Component, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Inject } from '@angular/core';
import { AuthService } from '../auth.service';
import { SendService } from '../send.service';

@Component({
  selector: 'dialog-send-to',
  templateUrl: './dialog-send-to.component.html',
  styleUrls: ['./dialog-send-to.component.css']
})
export class DialogSendToComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<DialogSendToComponent>, 
              public authService: AuthService, 
              public sendService: SendService,
              private db: AngularFireDatabase,
              @Inject(MAT_DIALOG_DATA) public data: any) {}

  codes:any[];
  //this should be inside the constructor @Inject(MAT_DIALOG_DATA) public data: DialogData
  onNoClick(): void {
    this.dialogRef.close();
  }

  send(i){
    console.log('Start send');
    console.log(this.codes[i]);
    console.log(this.data.text);
    let time = new Date().toString();
    let todo = {text:this.data.text, 'status':'',checked:false, createTime:time, key:this.data.key};
    let uid = this.codes[i].code;
    console.log(uid);
    console.log('End send');
    this.authService.user.subscribe(val =>{
      this.sendService.sendToDo(todo,uid,val.uid, val.displayName);
      let data = {nickname:this.codes[i].nickname, sent:true}
      this.dialogRef.close(data);
    });  


    //this is not supposed to work
    //this.db.list('user/'+uid +'/todos').push(todo);  
  }

  ngOnInit() {
    let uid = this.authService.user.subscribe(
      val =>{
        if(!val) return;        
        let uid = val.uid;
          this.db.list('/user/' + uid + '/codes').snapshotChanges().subscribe(
          codes =>{
            this.codes = [];
            codes.forEach(
              code => {
                let y = code.payload.toJSON();
                y["$key"] = code.key;
                this.codes.push(y);
              }
            )
            console.log(this.codes);
            console.log(this.data);
          }
        )
      }
    )    
  }

}
