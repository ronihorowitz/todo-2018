import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogSendToComponent } from './dialog-send-to.component';

describe('DialogSendToComponent', () => {
  let component: DialogSendToComponent;
  let fixture: ComponentFixture<DialogSendToComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogSendToComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogSendToComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
