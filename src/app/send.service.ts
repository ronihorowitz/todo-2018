import { Injectable } from '@angular/core';
import {Http} from '@angular/http';
import { environment } from '../environments/environment';


@Injectable({
  providedIn: 'root'
})
export class SendService {


  private url = environment.phppath + 'send.php';


  sendToDo(todo,uid, senderUid, name){
    console.log(uid);
    //JSON.stringify('');
    //add senderUID
    let body = {text:todo.text, key:todo.key, status:'', 'uid':uid,'senderUid':senderUid, 'name':name};
    this.http.post(this.url,body).subscribe(response =>{
      console.log(response);
    })
  }

  constructor(private http:Http) { }
}
