import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';

@Component({
  selector: 'nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {
  
 

  toRegister(){
    this.router.navigate(['/register']);
  }
  toCodes(){
    this.router.navigate(['/codes']);
  } 
  
  logout(){
    this.authService.logout();
  }

  constructor(private router:Router, public authService: AuthService ) { }

  ngOnInit() {
  }

}
