import { Component, OnInit } from '@angular/core';
//import { AngularFireDatabase } from 'angularfire2/database';
import { AuthService } from '../auth.service';

@Component({
  selector: 'register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  email: string;
  password: string;
  name: string;

  constructor(public authService: AuthService ) {}

  signup() {
    this.authService.signup(this.email, this.password, this.name);
    this.email = this.password = '';
  }

  ngOnInit() {
  }

}
