import { Injectable } from '@angular/core';
import {Http} from '@angular/http';
import { environment } from '../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SendbackService {

  //private url = 'http://localhost/sendfire/sendback.php';
  
  private url = environment.phppath + 'sendback.php';

  //should update this 
  sendChecked(todo){
    let body = todo;
    console.log("in send back service")
    console.log(body);
    this.http.post(this.url,body).subscribe(response =>{
      console.log(response);
    })
  }  

  constructor(private http:Http) { }
}
