import { AuthService } from './../auth.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  email;
  password;
  name;

  login(){
    this.authService.login(this.email, this.password);
    console.log(this.name);
    this.email = this.password = '';
  }


  constructor(public authService: AuthService) { }

  ngOnInit() {
  }

}
