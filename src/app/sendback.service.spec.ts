import { TestBed, inject } from '@angular/core/testing';

import { SendbackService } from './sendback.service';

describe('SendbackService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SendbackService]
    });
  });

  it('should be created', inject([SendbackService], (service: SendbackService) => {
    expect(service).toBeTruthy();
  }));
});
