import { Injectable } from '@angular/core';
import { AngularFireDatabase} from '@angular/fire/database';
import { AngularFireAuth} from '@angular/fire/auth';
import * as firebase from 'firebase/app';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';


@Injectable()
export class AuthService {
  
  user: Observable<firebase.User>;

  constructor(private firebaseAuth: AngularFireAuth, private db: AngularFireDatabase, private router:Router){
    this.user = firebaseAuth.authState;
  }

  signup(email: string, password: string, name:string) {
    this.firebaseAuth
      .auth
      .createUserWithEmailAndPassword(email, password)
      .then(value => {
        console.log('Success! ', value.user.uid);
        value.user.updateProfile({displayName:name,photoURL:''});
        let uid = value.user.uid;
        let ref = this.db.database.ref('/');        
        ref.child('user').child(uid).set({name:name});
      }).then(value => {
        this.router.navigate(['/']);
      })
      .catch(err => {
        console.log('Something went wrong:',err.message);
      });    
  }

  login(email: string, password: string) {
    this.firebaseAuth
      .auth
      .signInWithEmailAndPassword(email, password)
      .then(value => {
        console.log('Nice, it worked! ');
      }).then(value=>{
        this.router.navigate(['/']);
      })
      .catch(err => {
        console.log('Something went wrong:',err.message);
      });
  }

  logout() {   
    this.firebaseAuth
      .auth
      .signOut().then(() =>{
        this.router.navigate(['/login']);
      }       
    ); 
  }
  


}
