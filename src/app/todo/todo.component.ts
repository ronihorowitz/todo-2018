import { DialogSendToComponent } from './../dialog-send-to/dialog-send-to.component';
import { Component, OnInit, OnDestroy, Input, Output, EventEmitter } from '@angular/core';
import { MatDialog } from '@angular/material';
import { AuthService } from '../auth.service';
import { AngularFireDatabase, AngularFireList } from '@angular/fire/database';

@Component({
  selector: 'todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.css']
})
export class TodoComponent implements OnInit {


    @Input() data:any;
  
    @Output()
    todoAdded: EventEmitter<any> = new EventEmitter<any>();
    @Output()
    todoEdited: EventEmitter<any> = new EventEmitter<any>();
    @Output()
    todoDeleted: EventEmitter<any> = new EventEmitter<any>();
    @Output()
    toBeThrown: EventEmitter<any> = new EventEmitter<any>();  
    @Output()
    done: EventEmitter<any> = new EventEmitter<any>();
  
  
    text;
    $key;
    checkboxFlag: boolean;
    status;
    buttonText = "Add"
    sent = false;
    received = false; 
    nickname;

    dialogRef;
 
  
    showInterface = false; 
  
    show(){
        this.showInterface = true;
    };
  
    hide(){
      this.showInterface = false;
    }
  
      
    addORUpdateTodo(){
      this.status='';
      if(this.buttonText == 'Add'){
        this.todoAdded.emit(this.text);
      }else{
        let todo = new Array(); 
        todo['text'] = this.text;
        todo['key'] = this.$key;
        console.log(todo);
        this.todoEdited.emit(todo);
      }
        
    }
  
    deleteTodo(){
      //isn't the key enough? 
      let todo = new Array(); 
      todo['text'] = this.text;
      todo['key'] = this.$key;
      this.todoDeleted.emit(todo);
    }
  
    
    changeToEdit(){
      this.buttonText = "Save";
      this.status='edit';
    }
  
    checkChange(){
      let todo = new Array(); 
      todo['check'] = this.checkboxFlag;
      todo['key'] = this.$key; 
      this.done.emit(todo);    
    }

    send(){
      console.log("send operated");
      this.dialogRef = this.dialog.open(DialogSendToComponent, {
          width: '400px',
          height: '200px',
          data:{key:this.$key,text:this.text}
      }).afterClosed().
      subscribe(data =>{
        console.log('The dialog was closed');
        console.log(data);
        this.sent = data.sent;
        this.nickname = data.nickname;
        let time = new Date().toString();
        this.authService.user.subscribe(
          val =>{
            let uid = val.uid;
            this.db.list('user/'+uid +'/todos').update(this.$key,{'sent':true,'nickname':this.nickname,updateTime:time, 'checked':false});
          }
        )          
      })
    }

  

 
    constructor(private dialog:MatDialog, private authService:AuthService, private db: AngularFireDatabase  ) { }
  
    ngOnInit() {
      this.$key = this.data.$key;
      this.text = this.data.text;
      this.checkboxFlag = this.data.checked;
      this.status = this.data.status;

      if(this.data.sent){
        this.sent = true;
        this.nickname = this.data.nickname;
      }else if(this.data.from) {
        this.received = true; 
        this.nickname = this.data.from;
      }
    }
  
    ngOnDestroy(){
      //console.log("Actually deleted = "+ this.id);
    }

}





