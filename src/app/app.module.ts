//ng build --base-href=/ex2018/
import { SendService } from './send.service';
import { SendbackService } from './sendback.service';
import { AuthService } from './auth.service';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {Routes, RouterModule} from '@angular/router';
import {HttpModule} from '@angular/http';




import { NgModule } from '@angular/core';


import { AngularFireModule } from '@angular/fire';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { environment } from '../environments/environment';

import { FormsModule } from '@angular/forms';
import {ReactiveFormsModule} from '@angular/forms';

import {MatButtonModule, MatCheckboxModule} from '@angular/material';
import {MatMenuModule} from '@angular/material/menu';
import {MatInputModule} from '@angular/material/input';
import {MatCardModule} from '@angular/material/card';
import {MatIconModule} from '@angular/material/icon';
import {MatDialogModule} from '@angular/material/dialog';

import { AppComponent } from './app.component';
import { NavComponent } from './nav/nav.component';
import { TodosComponent } from './todos/todos.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { CodesComponent } from './codes/codes.component';
import { MainComponent } from './main/main.component';
import { TodoComponent } from './todo/todo.component';
import { DialogSendToComponent } from './dialog-send-to/dialog-send-to.component';

@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    TodosComponent,
    LoginComponent,
    RegisterComponent,
    CodesComponent,
    MainComponent,
    TodoComponent,
    DialogSendToComponent,
  ],
  entryComponents:[
    DialogSendToComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatCheckboxModule,
    MatMenuModule,
    MatInputModule,
    MatCardModule,
    MatIconModule,
    MatDialogModule,
    FormsModule,
    ReactiveFormsModule,  
    HttpModule,    
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule, // for database
    AngularFireAuthModule,    
    RouterModule.forRoot([
      {path:'',component:TodosComponent},
      {path:'register',component:RegisterComponent},
      {path:'login',component:LoginComponent},
      {path:'codes',component:CodesComponent},
      {path:'**',component:TodosComponent},
    ])    
  ],
  providers: [
    AuthService,
    SendService,
    SendbackService
  ], 
  bootstrap: [AppComponent]
})
export class AppModule { }
